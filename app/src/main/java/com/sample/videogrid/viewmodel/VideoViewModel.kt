package com.sample.videogrid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sample.videogrid.model.TagVideo
import com.sample.videogrid.repo.common.DataListOrException
import com.sample.videogrid.repo.video.VideoListOrException
import com.sample.videogrid.repo.video.VideoOrException
import com.sample.videogrid.repo.video.VideoRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class VideoViewModel : ViewModel(), KoinComponent {

    private val videoRepo by inject<VideoRepository>()

    // Fetch videos as per theme-collection-id
    fun getThemeVideos(collectionId: String): LiveData<VideoListOrException> {
        val ld: LiveData<VideoListOrException> = videoRepo.getThemeVideos(collectionId)
        return Transformations.map(ld) { results ->
            val filteredData = results.data?.filter { !it.isDeleted } as MutableList
            filteredData.sortByDescending { it.downloadCount }
            VideoListOrException(filteredData, results.exception)
        }
    }

    // Fetch video-ids as per tag-collection-id
    fun getTagVideoIds(tagId: String): LiveData<DataListOrException<TagVideo, Exception>> {
        return videoRepo.getTagVideoIds(tagId)
    }

    // Fetch video as per video-id
    fun getVideoById(id: String): LiveData<VideoOrException> {
        val ld = videoRepo.getVideoLiveData(id)
        return Transformations.map(ld) {
            VideoOrException(it.data, it.exception)
        }
    }
}