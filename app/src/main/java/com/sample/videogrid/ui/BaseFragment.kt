package com.sample.videogrid.ui

import android.os.Bundle
import android.view.View
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.sample.videogrid.R
import com.sample.videogrid.model.Video
import com.sample.videogrid.viewmodel.VideoViewModel

/**
 * A simple [Fragment] subclass
 * to write common code of all the fragments
 *
 */
abstract class BaseFragment(fragmentLayout: Int) : Fragment(fragmentLayout) {

    companion object {
        val videoList: MutableList<Video> by lazy { mutableListOf<Video>() }
    }

    lateinit var progressBar: ContentLoadingProgressBar

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private fun setDefaultSystemUI() {
        view?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setDefaultSystemUI()
    }

    abstract fun notifyAdapter()

    fun fetchVideoContent() {
        // Initiate fetching of videos
        val videoViewModel = VideoViewModel()

        val videoIdLiveData =
            videoViewModel.getTagVideoIds(resources.getString(R.string.app_collection_id))
        videoIdLiveData.observe(viewLifecycleOwner, Observer { results ->
            if (results.data != null) {
                videoIdLiveData.removeObservers(viewLifecycleOwner)
                val filteredData = results.data.filter { it.isActive } as MutableList

                for (tagVideo in filteredData) {
                    val videoData = videoViewModel.getVideoById(tagVideo.videoId)
                    videoData.observe(viewLifecycleOwner, Observer { result ->
                        if (result.data != null) {
                            videoData.removeObservers(viewLifecycleOwner)
                            val index = videoList.indexOf(result.data)
                            if (index >= 0) {
                                videoList[index] = result.data
                            } else {
                                videoList.add(result.data)
                            }
                            videoList.sortByDescending { it.downloadCount }
                            notifyAdapter()
                            if (videoList.isNotEmpty()) {
                                progressBar.visibility = View.GONE
                            }
                        }
                    })
                }
            }
        })
    }
}
