package com.sample.videogrid.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.videogrid.R
import com.sample.videogrid.adapter.HomeGridRecyclerAdapter
import com.sample.videogrid.config.EqualSpaceItemDecorator
import kotlinx.android.synthetic.main.fragment_home.*
import kotlin.random.Random


class HomeFragment : BaseFragment(R.layout.fragment_home) {

    private var firstVisibleIndex = 0
    private var lastVisibleIndex = 5
    private var currentPlayerIndex = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        home_loader.visibility = View.VISIBLE
        homeRecyclerGrid.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = HomeGridRecyclerAdapter(this@HomeFragment, videoList)
            addItemDecoration(EqualSpaceItemDecorator(2))
        }
        updateGridPlayer()

        homeRecyclerGrid.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    updateGridPlayer()
                }
            }
        })
        progressBar = home_loader
        fetchVideoContent()
    }

    private fun updateGridPlayer() {
        if (homeRecyclerGrid?.layoutManager != null) {
            firstVisibleIndex =
                (homeRecyclerGrid?.layoutManager as GridLayoutManager).findFirstCompletelyVisibleItemPosition()
            lastVisibleIndex =
                (homeRecyclerGrid?.layoutManager as GridLayoutManager).findLastCompletelyVisibleItemPosition()
        }
        if (firstVisibleIndex < 0 || lastVisibleIndex < 0) {
            firstVisibleIndex = 0
            lastVisibleIndex = 5
        }
        currentPlayerIndex = Random.nextInt(firstVisibleIndex, lastVisibleIndex)
        (homeRecyclerGrid?.adapter as HomeGridRecyclerAdapter).setCurrentPlayerIndex(currentPlayerIndex)
        notifyAdapter()
    }

    override fun notifyAdapter() {
        homeRecyclerGrid?.adapter?.notifyDataSetChanged()
    }

    override fun onDestroyView() {
        homeRecyclerGrid?.adapter = null
        super.onDestroyView()
    }
}