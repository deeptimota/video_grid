package com.sample.videogrid

import androidx.multidex.MultiDexApplication
import com.sample.videogrid.di.allModules
import com.bumptech.glide.Glide
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.unloadKoinModules

open class BaseApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(allModules)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        unloadKoinModules(allModules)
    }

    /**
     * Release memory when the UI becomes hidden or when system resources become low.
     * @param level the memory-related event that was raised.
     */
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)

        Glide.get(this).trimMemory(level)
        Glide.get(this).clearMemory()
        System.gc()
    }
}