package com.sample.videogrid

class Constants {
    companion object {
        const val ACTION_PLAYER_STARTED = "playerStarted"
        const val ACTION_PLAYER_ERROR = "playerError"
    }
}