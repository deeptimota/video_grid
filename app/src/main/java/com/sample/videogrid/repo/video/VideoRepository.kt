/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sample.videogrid.repo.video

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.sample.videogrid.localstorage.PreferenceHelper
import com.sample.videogrid.model.TagVideo
import com.sample.videogrid.model.Video
import com.sample.videogrid.repo.common.DataListOrException
import com.sample.videogrid.repo.common.DataOrException
import com.sample.videogrid.repo.common.DeserializeDataSnapshotTransform
import com.sample.videogrid.repo.common.DeserializeQuerySnapshotTransform
import com.sample.videogrid.repo.common.livedata.RealtimeDatabaseQueryLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.koin.core.KoinComponent
import org.koin.core.inject

class VideoRepository : KoinComponent {

    private val database by inject<FirebaseDatabase>()
    private val preferenceHelper by inject<PreferenceHelper>()

    private val videosLiveRef = database.getReference("videos")

    private var forceOnlineRequested = false
    private val videoSnapshotDeserializer = VideoSnapshotDeserializer()

    private fun verifyServerFetchTimings() {
        val lastTime = preferenceHelper.getDataFetchTime(PreferenceHelper.TIME_VIDEOS)

        if (System.currentTimeMillis() < lastTime + PreferenceHelper.TIME_DURATION_SHORT) {
            forceOnlineRequested = false
        } else {
            forceOnlineRequested = true
            preferenceHelper.writeDataFetchTime(
                PreferenceHelper.TIME_VIDEOS,
                System.currentTimeMillis()
            )
        }
    }

    /**
     * Gets a LiveData object from this repo that reflects the current value of
     * a single Video, given by its videoId.
     */
    fun getVideoLiveData(videoId: String): LiveData<VideoOrException> {
        val videoRef = videosLiveRef.child(videoId)
        val liveData = RealtimeDatabaseQueryLiveData(videoRef, forceOnlineRequested)
        return Transformations.map(
            liveData,
            DeserializeDataSnapshotTransform(
                videoSnapshotDeserializer
            )
        )
    }

    /**
     * Gets LiveData objects from this repo, belonging to specific collection-id.
     */
    fun getThemeVideos(themeId: String): LiveData<VideoListOrException> {
        verifyServerFetchTimings()
        val query = videosLiveRef.orderByChild("themeId").equalTo(themeId)
        val liveData = RealtimeDatabaseQueryLiveData(query, forceOnlineRequested)
        return Transformations.map(
            liveData,
            DeserializeQuerySnapshotTransform(
                videoSnapshotDeserializer
            )
        )
    }

    fun getTagVideoIds(tagId: String): LiveData<DataListOrException<TagVideo, Exception>> {
        verifyServerFetchTimings()
        val query = database.getReference("tagVideos/$tagId")
        val liveData = RealtimeDatabaseQueryLiveData(query, forceOnlineRequested)
        return Transformations.map(
            liveData,
            DeserializeQuerySnapshotTransform(
                TagVideoSnapshotDeserializer()
            )
        )
    }

    fun updateVideoDownloadCount(videoId: String) {
        database.goOnline()
        val videoRef = videosLiveRef.child(videoId)
        videoRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val currentDownloadCountForVideo =
                    dataSnapshot.child("downloadCount").value as Long
                val currentDateTime = System.currentTimeMillis()
                //Because only download count path is writable so we need to write that specific value only, instead on updating whole videoId child
                videoRef.child("downloadCount").setValue(currentDownloadCountForVideo + 1)
                videoRef.child("modifiedDate").setValue(currentDateTime)
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
    }
}

typealias VideoOrException = DataOrException<Video, Exception>

typealias VideoListOrException = DataListOrException<Video, Exception>