package com.sample.videogrid.repo.video

import com.sample.videogrid.model.TagVideo
import com.sample.videogrid.repo.common.DataSnapshotDeserializer
import com.sample.videogrid.repo.common.Deserializer
import com.google.firebase.database.DataSnapshot

class TagVideoSnapshotDeserializer :
    DataSnapshotDeserializer<TagVideo> {

    override fun deserialize(input: DataSnapshot): TagVideo {
        val key = input.key
        val data = input.value
        return if (key != null && data is Boolean) {
            TagVideo(key, data)
        } else {
            throw Deserializer.DeserializerException("DataSnapshot value wasn't an object Map")
        }
    }
}