/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sample.videogrid.repo.video

import com.sample.videogrid.model.AlternateVideo
import com.sample.videogrid.model.Video
import com.sample.videogrid.repo.common.DataSnapshotDeserializer
import com.sample.videogrid.repo.common.Deserializer
import com.google.firebase.database.DataSnapshot

internal class VideoSnapshotDeserializer :
    DataSnapshotDeserializer<Video> {

    override fun deserialize(input: DataSnapshot): Video {
        val data = input.value
        return if (data is Map<*, *>) {
            val videoId = input.key!!

            val downloadCount = data["downloadCount"] as Long? ?: 0

            var isDeleted = data["isDeleted"] as Boolean? ?: false

            val name = data["name"]?.toString() ?: ""

            val previewVideo = data["previewVideo"]?.toString() ?: ""

            val thumbnail = data["thumbnail"]?.toString() ?: ""

            val video1080pShortLandscape = data["video1080pShortLandscape"]?.toString() ?: ""

            val video1080pShortPortrait = data["video1080pShortPortrait"]?.toString() ?: ""

            val video1080pLandscapePlus = data["video1080pLandscapePlus"]?.toString()

            val video1080pPortraitPlus = data["video1080pPortraitPlus"]?.toString()

            val video2kShortLandscape = data["video2kShortLandscape"]?.toString()

            val video2kShortPortrait = data["video2kShortPortrait"]?.toString()

            val video480pShort = data["video480pShort"]?.toString() ?: ""

            val video720pShortLandscape = data["video720pShortLandscape"]?.toString() ?: ""

            val video720pShortPortrait = data["video720pShortPortrait"]?.toString() ?: ""

            val video720pLandscapePlus = data["video720pLandscapePlus"]?.toString()

            val video720pPortraitPlus = data["video720pPortraitPlus"]?.toString()

            val videoSpeed = data["videoSpeed"] as Long? ?: 25

            val alternateVideos = if (data["alternateVideos"] is Map<*, *>) {
                val alternateVideosMap = HashMap<String, AlternateVideo>()
                val videosMap = data["alternateVideos"] as HashMap<String, HashMap<String, String>>
                for ((key, value) in videosMap) {
                    value["alternateLandscape720"]?.let {
                        value["alternatePortrait720"]?.let { it1 ->
                            value["alternateLandscape1080"]?.let { it2 ->
                                value["alternatePortrait1080"]?.let { it3 ->
                                    alternateVideosMap[key] =
                                        AlternateVideo(
                                            it,
                                            it1,
                                            it2,
                                            it3,
                                            value["alternateLandscape2k"],
                                            value["alternatePortrait2k"]
                                        )
                                }
                            }
                        }
                    }
                }
                alternateVideosMap
            } else {
                null
            }

            val tags = if (data["tags"] is Map<*, *>) {
                data["tags"] as HashMap<String, Boolean>
            } else {
                null
            }

            if (!tags.isNullOrEmpty()) {
                for (key in tags.keys) {
                    if (key == Video.TAG_VIDEO_PACK && tags[key] == true) {
                        isDeleted = true
                    }
                }
            }

            Video(
                videoId,
                downloadCount,
                isDeleted,
                name,
                previewVideo,
                thumbnail,
                video1080pShortLandscape,
                video1080pShortPortrait,
                video1080pLandscapePlus,
                video1080pPortraitPlus,
                video2kShortLandscape,
                video2kShortPortrait,
                video480pShort,
                video720pShortLandscape,
                video720pShortPortrait,
                video720pLandscapePlus,
                video720pPortraitPlus,
                videoSpeed.toInt(),
                alternateVideos,
                tags
            )
        } else {
            throw Deserializer.DeserializerException("DataSnapshot value wasn't an object Map")
        }
    }
}