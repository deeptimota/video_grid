/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sample.videogrid.repo.common.livedata

import android.os.Handler
import com.sample.videogrid.repo.common.DataOrException
import com.google.firebase.database.*
import org.koin.core.KoinComponent
import org.koin.core.inject

typealias DataSnapshotOrException = DataOrException<DataSnapshot?, DatabaseException?>

class RealtimeDatabaseQueryLiveData : LingeringLiveData<DataSnapshotOrException>,
    ValueEventListener, KoinComponent {

    private val query: Query
    private val displayPath: String

    private val database by inject<FirebaseDatabase>()
    private val connectionModeHandler = Handler()
    private var connectionModeTask: Runnable? = null
    private var isDataReceived = false

    constructor(ref: DatabaseReference, forceOnlineRequested: Boolean) {
        setConnectionMode(forceOnlineRequested)
        this.query = ref
        this.displayPath = refToPath(ref)
    }

    constructor(query: Query, forceOnlineRequested: Boolean) {
        setConnectionMode(forceOnlineRequested)
        this.query = query
        this.displayPath = "query@${refToPath(query.ref)}"
    }

    private fun setConnectionMode(forceOnlineRequested: Boolean) {
        if (forceOnlineRequested) {
            database.goOnline()
        } else {
            database.goOffline()
            connectionModeTask = Runnable {
                if (!isDataReceived) {
                    database.goOnline()
                    beginLingering()
                }
            }
            connectionModeHandler.postDelayed(connectionModeTask, 1500)
        }
    }

    private fun refToPath(ref: DatabaseReference): String {
        var r = ref
        val parts = mutableListOf<String>()
        while (r.key != null) {
            parts.add(r.key!!)
            r = r.parent!!
        }
        return parts.asReversed().joinToString("/")
    }

    override fun beginLingering() {
        query.addValueEventListener(this)
    }

    override fun endLingering() {
        query.removeEventListener(this)
    }

    override fun onDataChange(snap: DataSnapshot) {
        isDataReceived = true
        if (connectionModeTask != null) {
            connectionModeHandler.removeCallbacks(connectionModeTask)
        }
        value = DataSnapshotOrException(snap, null)
    }

    override fun onCancelled(e: DatabaseError) {
        value = DataSnapshotOrException(null, e.toException())
    }
}
