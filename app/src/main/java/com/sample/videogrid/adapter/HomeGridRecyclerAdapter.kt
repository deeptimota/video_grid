package com.sample.videogrid.adapter

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.sample.videogrid.R
import com.sample.videogrid.config.VideoPlayer
import com.sample.videogrid.model.Video
import com.sample.videogrid.ui.HomeFragment
import com.bumptech.glide.RequestManager
import com.google.android.exoplayer2.ui.PlayerView
import com.google.firebase.storage.StorageReference
import org.koin.core.KoinComponent
import org.koin.core.inject

class HomeGridRecyclerAdapter(
    homeFragment: HomeFragment,
    videoList: MutableList<Video>
) :
    RecyclerView.Adapter<HomeGridViewHolder>(), KoinComponent {

    private val fragment: HomeFragment = homeFragment
    private val items: List<Video> = videoList
    private val glide by inject<RequestManager>()
    private val firebaseStorageReference by inject<StorageReference>()
    private val videoPlayer by inject<VideoPlayer>()
    private var currentPlayerPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeGridViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.home_grid_view_item, parent, false)
        return HomeGridViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: HomeGridViewHolder, position: Int) {
        val video = items[position]

        video.thumbnail.let {
            if (it.isNotBlank()) {
                glide.load(firebaseStorageReference.child(it))
                    .centerCrop()
                    .into(holder.imageView)
            }
        }

        if (currentPlayerPosition >= 0 && position == currentPlayerPosition) {
            if (items[currentPlayerPosition].previewUri != null) {
                holder.playerView.player =
                    videoPlayer.initializePlayer(
                        fragment.requireContext(),
                        items[currentPlayerPosition].previewUri!!
                    )
                Handler().postDelayed({
                    holder.imageView.visibility = View.GONE
                }, 400)
            } else {
                firebaseStorageReference.child(items[currentPlayerPosition].video480pShort)
                    .downloadUrl
                    .addOnSuccessListener(fragment.requireActivity()) { uri ->
                        items[currentPlayerPosition].previewUri = uri

                        holder.playerView.player =
                            videoPlayer.initializePlayer(fragment.requireContext(), uri)
                        Handler().postDelayed({
                            holder.imageView.visibility = View.GONE
                        }, 200)
                    }
            }
        } else {
            holder.playerView.player = null
            holder.imageView.visibility = View.VISIBLE
        }
    }

    fun setCurrentPlayerIndex(index: Int) {
        currentPlayerPosition = index
    }
}

class HomeGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var imageView: AppCompatImageView = itemView.findViewById(R.id.gridImageView)
    var playerView: PlayerView = itemView.findViewById(R.id.gridPlayerView)
}