package com.sample.videogrid

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sample.videogrid.localstorage.PreferenceHelper
import com.google.firebase.auth.FirebaseAuth
import org.koin.android.ext.android.inject
import java.util.*

class SplashActivity : AppCompatActivity() {

    private val auth by inject<FirebaseAuth>()
    private val preferenceHelper by inject<PreferenceHelper>()

    override fun onCreate(savedInstanceState: Bundle?) {
        if (resources.getBoolean(R.bool.isTab)) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        }

        super.onCreate(savedInstanceState)

        // Setup for authentication
        auth.addAuthStateListener(authStateListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        auth.removeAuthStateListener(authStateListener)
    }

    private val authStateListener = FirebaseAuth.AuthStateListener { auth ->
        val loggedIn = auth.currentUser != null
        if (loggedIn) {
            if (preferenceHelper.readInt(PreferenceHelper.APP_VERSION_CODE) < BuildConfig.VERSION_CODE) {
                preferenceHelper.clear(PreferenceHelper.DEVICE_LANG)
                preferenceHelper.clear(PreferenceHelper.TIME_STATIC_TEXTS)
                preferenceHelper.clear(PreferenceHelper.TIME_VIDEOS)
                preferenceHelper.writeInt(
                    PreferenceHelper.APP_VERSION_CODE,
                    BuildConfig.VERSION_CODE
                )
            }

            if (preferenceHelper.readString(PreferenceHelper.DEVICE_LANG) != Locale.getDefault().language) {
                preferenceHelper.clear(PreferenceHelper.TIME_STATIC_TEXTS)
            }

            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        } else {
            auth.signInAnonymously()
        }
    }
}