package com.sample.videogrid.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AlternateVideo(
    var alternateLandscape720: String,
    var alternatePortrait720: String,
    var alternateLandscape1080: String,
    var alternatePortrait1080: String,
    var alternateLandscape2k: String? = null,
    var alternatePortrait2k: String? = null
) : Parcelable