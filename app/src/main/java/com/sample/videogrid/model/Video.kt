package com.sample.videogrid.model

import android.media.CamcorderProfile
import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Video(
    var videoId: String,
    var downloadCount: Long,
    var isDeleted: Boolean,
    var name: String,
    var previewVideo: String,
    var thumbnail: String,
    var video1080pShortLandscape: String,
    var video1080pShortPortrait: String,
    var video1080pLandscapePlus: String? = null,
    var video1080pPortraitPlus: String? = null,
    var video2kShortLandscape: String? = null,
    var video2kShortPortrait: String? = null,
    var video480pShort: String,
    var video720pShortLandscape: String,
    var video720pShortPortrait: String,
    var video720pLandscapePlus: String? = null,
    var video720pPortraitPlus: String? = null,
    var videoSpeed: Int = 25,
    var alternateVideos: HashMap<String, AlternateVideo>? = null,
    var tags: HashMap<String, Boolean>? = null
) : Parcelable {

    var previewUri: Uri? = null

    companion object {
        const val QUALITY_HD = "qualityHD"
        const val QUALITY_HQ = "qualityHQ"
        const val TAG_PREMIUM = "-M5HxAYP5N9Xja3rS6T3"
        const val TAG_VIDEO_PACK = "-M7Hy1XwxkpVAhxK1yQa"
    }

    fun isPremiumVideo(): Boolean {
        if (!tags.isNullOrEmpty()) {
            for (key in tags?.keys!!) {
                if (key == TAG_PREMIUM && tags!![key] == true) {
                    return true
                }
            }
        }
        return false
    }

    fun getVideoUrl(quality: String, resolution: Int, isLandscape: Boolean): String {
        return when (resolution) {
            CamcorderProfile.QUALITY_480P -> {
                video480pShort
            }
            CamcorderProfile.QUALITY_720P -> {
                when (quality) {
                    QUALITY_HQ -> {
                        if (isLandscape)
                            video720pLandscapePlus ?: video720pShortLandscape
                        else
                            video720pPortraitPlus ?: video720pShortPortrait
                    }
                    else -> {
                        if (isLandscape)
                            video720pShortLandscape
                        else
                            video720pShortPortrait
                    }
                }
            }
            CamcorderProfile.QUALITY_1080P -> {
                when (quality) {
                    QUALITY_HQ -> {
                        if (isLandscape)
                            video1080pLandscapePlus
                                ?: video1080pShortLandscape
                        else
                            video1080pPortraitPlus ?: video1080pShortPortrait
                    }
                    else -> {
                        if (isLandscape)
                            video1080pShortLandscape
                        else
                            video1080pShortPortrait
                    }
                }
            }
            CamcorderProfile.QUALITY_2160P -> {
                if (isLandscape)
                    video2kShortLandscape
                        ?: video1080pLandscapePlus ?: video1080pShortLandscape
                else
                    video2kShortPortrait ?: video1080pPortraitPlus
                    ?: video1080pShortPortrait
            }
            else -> previewVideo
        }
    }

    fun getAlternateVideoUrl(index: Int, resolution: Int, isLandscape: Boolean): String? {
        return getAlternateVideoByIndex(index)?.let {
            when (resolution) {
                CamcorderProfile.QUALITY_2160P -> {
                    if (isLandscape)
                        it.alternateLandscape2k ?: it.alternateLandscape1080
                    else
                        it.alternatePortrait2k ?: it.alternatePortrait1080
                }
                CamcorderProfile.QUALITY_1080P -> {
                    if (isLandscape)
                        it.alternateLandscape1080
                    else
                        it.alternatePortrait1080
                }
                else -> {
                    if (isLandscape)
                        it.alternateLandscape720
                    else
                        it.alternatePortrait720
                }
            }
        }
    }

    private fun getAlternateVideoByIndex(index: Int): AlternateVideo? {
        return alternateVideos?.values?.elementAt(index)
    }

    override fun equals(other: Any?): Boolean {
        if (other is Video && other.videoId == this.videoId) {
            return true
        }
        return false
    }
}