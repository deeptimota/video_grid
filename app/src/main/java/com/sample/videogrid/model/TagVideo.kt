package com.sample.videogrid.model

data class TagVideo(
    var videoId: String,
    var isActive: Boolean
)