package com.sample.videogrid.config

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.sample.videogrid.Constants
import com.sample.videogrid.R
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.drm.DrmSessionManager
import com.google.android.exoplayer2.drm.ExoMediaCrypto
import com.google.android.exoplayer2.extractor.Extractor
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.ParametersBuilder
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.FileDataSource
import com.google.android.exoplayer2.upstream.cache.*
import com.google.android.exoplayer2.util.Util

class VideoPlayer {
    companion object {
        private var cache: Cache? = null
        private var upstreamFactory: DataSource.Factory? = null
    }

    private var player: SimpleExoPlayer? = null

    // Internal methods
    fun initializePlayer(context: Context, videoUri: Uri): SimpleExoPlayer? {
        if (player == null) {
            val trackSelectorParameters = ParametersBuilder(context).build()
            val trackSelectionFactory =
                AdaptiveTrackSelection.Factory()    // ABR_ALGORITHM_DEFAULT (default) player configuration
            val trackSelector = DefaultTrackSelector(context, trackSelectionFactory)
            trackSelector.parameters = trackSelectorParameters
            val renderersFactory =
                DefaultRenderersFactory(context).setExtensionRendererMode(DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF)

            player = SimpleExoPlayer.Builder(context, renderersFactory)
                .setTrackSelector(trackSelector)
                .build()
            player?.addListener(object : Player.EventListener {
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    super.onPlayerStateChanged(playWhenReady, playbackState)
                    if (playbackState == Player.STATE_READY)
                        LocalBroadcastManager.getInstance(context)
                            .sendBroadcast(Intent(Constants.ACTION_PLAYER_STARTED))
                }

                override fun onPlayerError(error: ExoPlaybackException) {
                    super.onPlayerError(error)
                    LocalBroadcastManager.getInstance(context)
                        .sendBroadcast(Intent(Constants.ACTION_PLAYER_ERROR))
                }
            })
            player?.volume = 0.0f
            player?.playWhenReady = true

            if (cache == null) {
                cache =
                    SimpleCache(
                        context.externalCacheDir ?: context.cacheDir,
                        NoOpCacheEvictor(),
                        ExoDatabaseProvider(context)
                    )
            }

            if (upstreamFactory == null) {
                upstreamFactory =
                    DefaultDataSourceFactory(
                        context, DefaultHttpDataSourceFactory(
                            Util.getUserAgent(
                                context, context.resources.getString(
                                    R.string.app_name
                                )
                            )
                        )
                    )
            }
        }

        updateUri(player, videoUri)

        return player
    }

    private fun updateUri(exoPlayer: SimpleExoPlayer?, videoUri: Uri) {
        val mediaSource: MediaSource = createTopLevelMediaSource(videoUri) ?: return
        // Loops the video(s) indefinitely.
        val loopingSource = LoopingMediaSource(mediaSource)
        exoPlayer?.prepare(loopingSource)
    }

    private fun createTopLevelMediaSource(videoUri: Uri): MediaSource? {
        if (!Util.checkCleartextTrafficPermitted(videoUri)) {
            Log.e("Player", "Cleartext traffic not permitted")
            return null
        }
        return createLeafMediaSource(videoUri)
    }

    private fun createLeafMediaSource(uri: Uri): MediaSource? {
        val drmSessionManager: DrmSessionManager<ExoMediaCrypto> =
            DrmSessionManager.getDummyDrmSessionManager()
        val dataSourceFactory = CacheDataSourceFactory(
            cache,
            upstreamFactory,
            FileDataSource.Factory(),
            null,
            CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
            null
        )
        return ProgressiveMediaSource.Factory(
            dataSourceFactory, Mp4ExtractorsFactory()
        ).setDrmSessionManager(drmSessionManager).createMediaSource(uri)
    }

    fun stopPlayer() {
        player?.stop()
    }

    fun releasePlayer() {
        if (player != null) {
            player?.release()
            player = null
        }
    }

    private class Mp4ExtractorsFactory : ExtractorsFactory {
        override fun createExtractors(): Array<Extractor> {
            return arrayOf(Mp4Extractor())
        }
    }
}