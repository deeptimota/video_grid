package com.sample.videogrid.localstorage

import android.app.AlarmManager
import android.content.Context
import android.content.SharedPreferences
import android.media.CamcorderProfile
import com.sample.videogrid.model.Video

class PreferenceHelper {

    private val preferences: SharedPreferences

    constructor(preferences: SharedPreferences) {
        this.preferences = preferences
    }

    companion object {
        private const val SELECTED_VIDEO_QUALITY = "selected_video_quality"
        private const val SELECTED_VIDEO_RESOLUTION = "selected_video_resolution"
        private const val SELECTED_SHUFFLE_DURATION = "selected_shuffle_duration"
        private const val USER_DOWNLOAD_COUNT = "user_download_count"
        private const val USER_DOWNLOADS = "user_downloads"
        const val DEVICE_LANG = "device_lang"
        const val APP_VERSION_CODE = "app_version_code"
        const val TIME_STATIC_TEXTS = "time_static_texts"
        const val TIME_VIDEOS = "time_videos"
        const val TIME_DURATION_SHORT = AlarmManager.INTERVAL_DAY * 7
        const val TIME_DURATION_AVERAGE = AlarmManager.INTERVAL_DAY * 15
    }

    private fun getEditor(): SharedPreferences.Editor {
        return preferences.edit()
    }

    fun writeString(
        key: String,
        value: String
    ) {
        getEditor().putString(key, value).commit()
    }

    fun readString(
        key: String,
        defValue: String = ""
    ): String {
        return preferences.getString(key, defValue) ?: ""
    }

    fun writeInt(
        key: String,
        value: Int
    ) {
        getEditor().putInt(key, value).commit()
    }

    fun readInt(
        key: String,
        defaultValue: Int = 0
    ): Int {
        return preferences.getInt(key, defaultValue)
    }

    fun clear(key: String) {
        getEditor().remove(key).commit()
    }

    /**
     * Method to manage offline/online mode for fetching data
     * @param key identification for type of data
     * @param time time in millis, when data was last fetched as online
     */
    fun writeDataFetchTime(key: String, time: Long) {
        getEditor().putLong(key, time).commit()
    }

    fun getDataFetchTime(key: String): Long {
        return preferences.getLong(key, 0)
    }

    /**
     * Method to manage info-messages shown to user or not
     * @param key identification for type of information
     */
    fun writeInfoShown(key: String) {
        getEditor().putBoolean(key, true).commit()
    }

    fun getInfoShown(key: String): Boolean {
        return preferences.getBoolean(key, false)
    }

    /**
     * Method to identify default or manually over-ridden video quality
     * @return HD or HQ or 2K
     */
    fun readSelectedVideoQuality(): String {
        return readString(
            SELECTED_VIDEO_QUALITY,
            Video.QUALITY_HQ
        )
    }

    fun writeSelectedVideoQuality(selectedQuality: String) {
        writeString(SELECTED_VIDEO_QUALITY, selectedQuality)
    }

    /**
     * Method to identify best-suited or manually over-ridden video resolution
     * @return CamcorderProfile.QUALITY_480P or CamcorderProfile.QUALITY_720P or CamcorderProfile.QUALITY_1080P
     */
    fun readSelectedResolution(context: Context): Int {
        var defaultValue: Int = readInt(SELECTED_VIDEO_RESOLUTION)
        if (defaultValue == 0) {
            defaultValue = CamcorderProfile.QUALITY_480P
            writeInt(SELECTED_VIDEO_RESOLUTION, defaultValue)
        }

        return readInt(SELECTED_VIDEO_RESOLUTION, defaultValue)
    }

    fun writeSelectedResolution(selectedResolution: Int) {
        writeInt(SELECTED_VIDEO_RESOLUTION, selectedResolution)
    }

    /**
     * Method to identify the shuffle duration
     * @return duration in minutes
     */
    fun readSelectedShuffleDuration(): Int {
        return readInt(SELECTED_SHUFFLE_DURATION)
    }

    fun writeSelectedShuffleDuration(selectedDuration: Int) {
        writeInt(SELECTED_SHUFFLE_DURATION, selectedDuration)
    }

    fun clearSelectedShuffleDuration() {
        clear(SELECTED_SHUFFLE_DURATION)
    }

    /**
     * Method to manage download related details
     */
    fun readDownloadCount(): Int {
        return readInt(USER_DOWNLOAD_COUNT)
    }

    fun writeDownloadCount(count: Int) {
        writeInt(USER_DOWNLOAD_COUNT, count)
    }

    fun readDownloadsList(): String {
        return readString(USER_DOWNLOADS, "")
    }

    fun writeDownloadsList(listString: String) {
        writeString(USER_DOWNLOADS, listString)
    }
}