/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sample.videogrid.di

import android.content.Context
import com.sample.videogrid.R
import com.sample.videogrid.config.VideoPlayer
import com.sample.videogrid.localstorage.PreferenceHelper
import com.sample.videogrid.repo.video.VideoRepository
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

interface RuntimeConfig {
    var videoRepository: VideoRepository
}

class SingletonRuntimeConfig : RuntimeConfig {
    companion object {
        val instance = SingletonRuntimeConfig()
    }

    override var videoRepository: VideoRepository = videoRepositoryInstance
}

private val videoPlayer by lazy { VideoPlayer() }
private val videoRepositoryInstance by lazy { VideoRepository() }
private val glideRequestOptions by lazy {
    RequestOptions
        .placeholderOf(R.drawable.white_background)
        .error(R.drawable.white_background)
}

val appModule = module {
    single { videoPlayer }
    single { SingletonRuntimeConfig.instance as RuntimeConfig }
    factory { get<RuntimeConfig>().videoRepository }
    single {
        Glide.with(androidContext())
            .setDefaultRequestOptions(glideRequestOptions)
    }
    single {
        PreferenceHelper(
            androidContext().getSharedPreferences(
                "pref${androidContext().resources.getString(R.string.app_collection_id)}",
                Context.MODE_PRIVATE
            )
        )
    }
}

val firebaseModule = module {
    single(createdAtStart = true) {
        FirebaseApp.initializeApp(androidContext())
        val instance = FirebaseDatabase.getInstance()
        instance.setPersistenceEnabled(true)
        instance
    }
    single { FirebaseAuth.getInstance() }
    single { Firebase.storage.reference }
}

val allModules = listOf(firebaseModule, appModule)